package org.rockyang.mybatis.boot.demo.test.support.mapper;


import org.rockyang.mybatis.boot.demo.test.support.model.Order;
import org.rockyang.mybatis.plus.support.BaseMapper;

/**
 * @author yangjian
 */
public interface OrderMapper extends BaseMapper<Order> {

}
